<?php
get_header(); ?>
<script type="text/javascript">
//<![CDATA[
function imgLoaded(img){  
    $(img).parent().addClass('loaded');
  }
//]]>
</script>
<?php $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); ?>
<?php
      global $post;

      if(get_query_var('paged')) $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      if(get_query_var('page')) $paged = (get_query_var('page')) ? get_query_var('page') : 1;

      $args = array(
      'post_type' => array('automjete'),
      'post_status' => 'publish',
      'tax_query' => array(
        array(
          'taxonomy' => $term->taxonomy,
        'terms' => array($term->term_id),
        'field' => $term->slug,
        'operator' => 'IN'
        )
      ),
      'posts_per_page' => 32,
      'orderby' => 'date',
      'order' => 'DESC',
      'paged' => $paged
      );
      $wp_query = null;
      $wp_query = new WP_Query($args);
      $total_pages = $wp_query->max_num_pages;
      ?>

<section class="post-content-special">
<div class="row">
  <div class="container">
    <div class="span12 leftZero">
    <div class="wrapper wf">
      <!-- BEGIN CONTROLS -->
      <nav class="controls just">
        <div class="group" id="Sorts">
          <div class="button active" id="ToList"><i></i>Listë</div>
          <div class="button" id="ToGrid"><i></i>Rrjetë</div>
        </div>
        <div class="group btn btn-danger" id="Filters">
          <div class="drop_down wf">
            <span class="anim150">Lloji i mjetit</span>
            <ul class="anim250">
              <li class="active" data-filter="all" data-dimension="region">Të gjitha</li>
              <?php
              $args = array('taxonomy' => 'lloji', 'hide_empty' => false);
              $terms = get_terms('lloji', $args);
              // var_dump($terms);
              foreach($terms as $term) {
              ?>
              <li data-filter="<?php echo strtolower($term->name); ?>" data-dimension="region"><?php echo ucfirst($term->name); ?></li>
               <?php
              }
              ?>
            </ul>
          </nav><!-- END CONTROLS -->     
      <!-- BEGIN PARKS -->
      <ul id="Parks" class="just leftZero">
        <!-- "TABLE" HEADER CONTAINING SORT BUTTONS (HIDDEN IN GRID MODE)-->
        <div class="list_header">
          <div class="meta name active desc" id="SortByName">
            Rendit shpalljet sipas alfabetit &nbsp;
            <span class="sort anim150 asc active" data-sort="data-name" data-order="desc"></span>
            <span class="sort anim150 desc" data-sort="data-name" data-order="asc"></span>  
          </div>
          <div class="meta region">Lloji</div>
          <div class="meta area" id="SortByArea">
            Rendit sipas çmimit &nbsp;
            <span class="sort anim150 asc" data-sort="data-area" data-order="asc"></span>
            <span class="sort anim150 desc" data-sort="data-area" data-order="desc"></span>
          </div>
        </div>
        
        <!-- FAIL ELEMENT -->
        
        <div class="fail_element anim250">Nuk ka asnjë shpallje me kriteret që keni kërkuar !</div>
          <?php if ( $wp_query->have_posts() ) : ?>
          <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
          <?php 
              $auto_price = get_post_meta($post->ID, 'ac_cmimi_auto', true);
              $cmimi_ndryshuar = get_post_meta($post->ID,'ac_cmimi_ndryshuar_auto', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              $cmimi_test = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              if(is_numeric($auto_location)) {
                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                $auto_location = $lok['name'];
              }
              $auto_status = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_year = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
              $kudos = get_kudos_count($post->ID);
              $status = array();
              if(isset($auto_status)&& $auto_status != '') {
	              foreach($auto_status as $s) {
	                  switch($s) {
	                      case "0":
	                      $status[] = "Me marrëveshje";
	                      break;
	
	                      case "1":
	                      $status[] = "Çmimi fiks";
	                      break;
	
	
	                      case "2":
	                      $status[] = "Ndërroj";
	                  }
	              }
              }
            ?>

        <!-- BEGIN LIST OF PARKS (MANY OF THESE ELEMENTS ARE VISIBLE ONLY IN LIST MODE)-->
        <?php 
        $terms_list = get_the_term_list($post->ID, 'lloji', '', '', '');

        ?>
        <li class="mix <?php echo strtolower(strip_tags($terms_list)); ?>" data-name="<?php the_title(); ?>" data-area="<?php echo $auto_price; ?>">
          <div class="meta name">
            <div class="img_wrapper">
              <?php
                //$url = wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID), 'medium');
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
                $url = $thumb[0];
              ?>
              <a href="<?php the_permalink(); ?>"><div class="img_wrapper"><img src="<?php echo $url; ?>" onload="imgLoaded(this)"/></div></a>
            </div>
            <div class="titles">
              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <p><em><?php echo ac_limit_excerpt(); ?></em></p>
              <div class="main-info">
              <ul class="leftZero">
                <li><i class="icon icon-eye-open"></i><?php echo wpp_get_views($post->ID);?></li>
                <li><i class="icon icon-thumbs-up"></i><?php echo $kudos; ?></li>
                <li><i class="icon icon-map-marker"></i><?php echo ucfirst($auto_location); ?></li>
                <li><i class="icon icon-time"></i></i><?php the_time('H:i:s')?> | <?php echo get_the_date( 'd-m-Y' ); ?></li>
              </ul>
            </div>
            </div>
          </div>
          <div class="meta region">
            <p><?php echo get_the_term_list($post->ID, 'lloji', '','',''); ?></p>
          </div>
          <div class="meta area">
            <div>
              <p><?php echo $auto_price; ?>&euro;</p>
            </div>
          </div>

          <div class="meta grid_price_status">
                <?php 
                if(isset($status)) {
                    foreach($status as $price_status){
                        echo 
                        '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                   }
                }
                ?>
          </div>

          <div class="meta extra-info">
              <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
              <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
          </div>
        </li>
        
        <?php 
      endwhile;
        ?>
        
        <?php
        else:
        ?>
      <p>Nuk ka asnje post.</p>
      <?php
      endif;
      wp_reset_query();
      ?>
      </ul>
    </div> <!-- END DEMO WRAPPER -->
    <?php 
    if(function_exists("ac_pagination")) {
      ac_pagination($total_pages);
    } 
  ?>
</div><!--/span12 -->
</div><!-- /container -->
</div><!-- /row -->
</section><!-- /post-content -->
<?php get_footer(); ?>
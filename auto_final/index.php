<?php get_header(); ?>
<section class="main-content">
    <div class="container">
        <div class="span12 leftZero">
            <div class="span7 search-container leftZero">
                <?php include(TEMPLATEPATH.'/search-cars-form.php'); ?>
            </div><!--/span7 -->
            <div class="span5 full-navigation">
                <?php include(TEMPLATEPATH.'/search-field.php'); ?>
            </div><!-- /span6 -->
        </div><!-- /span12 -->
    </div><!-- /container -->
</section><!-- /main-content -->


<section class="banner-container">
    <div class="row">
        <div class="container">
            <div class="span12 banner">
                <h3>Reklamoni këtu</h3>
            </div><!-- /span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /banner-container -->

<section class="post-content post-premium-special">
    <div class="row">
        <div class="container">
            <div class="span12 leftZero" id="main" >
                <h3 class="page-header"><span>Speciale</span></h3>
                <div class="auto_container_grid">
                    <div class="row">
                        <?php 
                        //marrim 6 postimet e fundit sipas kohen ne te cilen jane publikuar.
                        $query = array(
                            'post_type' => 'automjete',
                            'posts_per_page' => 8,
                            'post_status' => 'publish',
                            'meta_key' => 'ac_speciale',
                            'meta_value' =>  1,
                            'meta_compare' => '=',
                            'orderby' => 'date',
                            'order' => 'DESC'
                        );
                        $featured_posts = new WP_Query($query);

                        if($featured_posts->have_posts()) : 
                            while($featured_posts->have_posts()) : 
                                $featured_posts->the_post();
                            //te dhenat
                            $auto_type = get_post_meta($post->ID, 'ac_tipi_automjetit', true);
                            $auto_model = get_post_meta($post->ID, 'ac_modeli', true);
                            $auto_price = get_post_meta($post->ID, 'ac_cmimi_auto', true);
                            $auto_location = get_post_meta($post->ID, 'lokacioni', true);
                            $auto_status = get_post_meta($post->ID, 'ac_cmimi_test', true);
                            $auto_year = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
                            $status = array();
                            foreach((array)$auto_status as $s) {
                                switch($s) {
                                    case "0":
                                    $status[] = "Me marrëveshje";
                                    break;

                                    case "1":
                                    $status[] = "Çmimi fiks";
                                    break;


                                    case "2":
                                    $status[] = "Ndërroj";
                                }
                            }

                            

                            if(is_numeric($auto_location)) {
                                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                                $auto_location = $lok['name'];
                            }
                            $auto_trans = get_post_meta($post->ID, 'transmisioni', true);
                            if(is_numeric($auto_trans)) {
                                $trans = get_term_by('id', $auto_trans, 'transmisioni', 'ARRAY_A');
                                $auto_trans = $trans['name'];
                            }
                            $auto_km = get_post_meta($post->ID, 'ac_km_kaluara', true);
                        ?>
                        <div class="span3">
                            <div class="auto_container">
                                <div class="image">
                                    <div class="content">
                                        <a href="<?php the_permalink(); ?>"></a>
                                        <?php if(has_post_thumbnail()): ?>
                                        <?php the_post_thumbnail('medium');?>
                                        <?php else: ?>
                                        <img src="<?php echo IMAGESROOT; ?>/default.png" width="300" height="250" alt="">
                                        <?php endif; ?>
                                    </div><!-- /.content -->

                                    <div class="price_type">
                                        <?php echo $auto_price; ?> &euro;
                                    </div><!-- /.Ndrroj-Shitet -->
    
                                    <div class="price">
                                        <?php 
                                        if(isset($status)) {
                                            foreach($status as $price_status){
                                                echo 
                                                '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                                           }
                                        }
                                        ?>
                                    </div><!-- /.price -->

                                </div><!-- /.image -->

                                <div class="title">
                                    <h3>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                </div><!-- /.title -->

                                <div class="extra-info">
                                    <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
                                    <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
                                </div>
                            </div><!-- /auto_container -->       
                        </div><!-- /span3 -->
                        <?php
                        endwhile;
                        else:
                        ?>
                        <p>Nuk ka të dhena.</p>
                        <?php
                          endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div><!-- /.properties-grid -->
            </div><!--/span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /post-content -->

<section class="post-content">
    <div class="row">
        <div class="container">
            <div class="span12 leftZero" id="main" >
                <h3 class="page-header"><span>Shpalljet e fundit</span></h3>
                <div class="auto_container_grid">
                    <div class="row">
                        <?php 
                        //marrim 6 postimet e fundit sipas kohen ne te cilen jane publikuar.
                        $query = array(
                            'post_type' => 'automjete',
                            'posts_per_page' => 12,
                            'post_status' => 'publish',
                            'orderby' => 'date',
                            'order' => 'DESC'
                        );
                        $latest_posts = new WP_Query($query);

                        if($latest_posts->have_posts()) : 
                            while($latest_posts->have_posts()) : 
                                $latest_posts->the_post();
                            //te dhenat
                            $auto_type = get_post_meta($post->ID, 'ac_tipi_automjetit', true);
                            $auto_model = get_post_meta($post->ID, 'ac_modeli', true);
                            $auto_price = get_post_meta($post->ID, 'ac_cmimi_auto', true);
                            $auto_location = get_post_meta($post->ID, 'lokacioni', true);
                            $auto_status = get_post_meta($post->ID, 'ac_cmimi_test', true);
                            $auto_year = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
                            $status = array();
                            foreach((array)$auto_status as $s) {
                                switch($s) {
                                    case "0":
                                    $status[] = "Me marrëveshje";
                                    break;

                                    case "1":
                                    $status[] = "Çmimi fiks";
                                    break;


                                    case "2":
                                    $status[] = "Ndërroj";
                                }
                            }
                            

                            if(is_numeric($auto_location)) {
                                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                                $auto_location = $lok['name'];
                            }
                            $auto_trans = get_post_meta($post->ID, 'transmisioni', true);
                            if(is_numeric($auto_trans)) {
                                $trans = get_term_by('id', $auto_trans, 'transmisioni', 'ARRAY_A');
                                $auto_trans = $trans['name'];
                            }
                            $auto_km = get_post_meta($post->ID, 'ac_km_kaluara', true);
                        ?>
                        <div class="span3">
                            <div class="auto_container">
                                <div class="image">
                                    <div class="content">
                                        <a href="<?php the_permalink(); ?>"></a>
                                        <?php if(has_post_thumbnail()): ?>
                                        <?php the_post_thumbnail('medium');?>
                                        <?php else: ?>
                                        <img src="<?php echo IMAGESROOT; ?>/default.png" width="300" height="250" alt="">
                                        <?php endif; ?>
                                    </div><!-- /.content -->

                                    <div class="price_type">
                                        <?php echo $auto_price; ?> &euro;
                                    </div><!-- /.Ndrroj-Shitet -->
    
                                    <div class="price">
                                        <?php 
                                        if(isset($status)) {
                                            foreach($status as $price_status){
                                                echo 
                                                '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                                           }
                                        }
                                        ?>
                                    </div><!-- /.price -->

                                </div><!-- /.image -->

                                <div class="title">
                                    <h3>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                </div><!-- /.title -->

                                <div class="extra-info">
                                    <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
                                    <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
                                </div>
                            </div><!-- /auto_container -->       
                        </div><!-- /span3 -->
                        <?php
                        endwhile;
                        else:
                        ?>
                        <p>Nuk ka të dhena.</p>
                        <?php
                          endif;
                        wp_reset_query();
                        ?>
                    </div>
                </div><!-- /.properties-grid -->
            </div><!--/span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /post-content -->

<section>
    <div class="row">
        <div class="container">
            <div class="span12 leftZero" id="main" >
            <h3 class="page-header"><span>Postimet e popullarizuara</span></h3>
            <?php 
            wpp_get_mostpopular('post_type=automjete&limit=8&range=monthly&order_by=views&stats_views=1&stats_comments=0&thumbnail_width=270&thumbnail_height=165&wpp_start="<div class=auto_container_grid id=auto_statistics><div class=row>"&wpp_end="</div></div></div>"&post_html="<div class=span3><div class=auto_container><div class=image><div class=content>{thumb}</div></div><div class=title><h3><a href={url}>{text_title}</a></h3></div><div class=views><span class=label-info id=stats>{stats}</span></div></div></div>"'); 
            ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
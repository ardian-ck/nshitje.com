<?php
/**
 * Template Name:Konfirmo Fshirjen
 */
?>
<?php get_header(); ?>

<section class="listing-container">
    <div class="row">
        <div class="container">
            <div class="span12 listings">
				<?php 
				global $wpdb;

				$error = array();

				if(isset($_GET['post']) && isset($_GET['id']) && strlen($_GET['id']) == 32) {
					$post_id = intval($_GET['post']);
                    $private_id = ($post_id - 825920496) / 1498158;
					$post_key = $_GET['id'];

					//marrim te dhenat per postin me id dhe post key nga url.
					$args = array(
						'post_type' => 'automjete',
						'meta_key' => 'ac_timestamp',
						'meta_value' => $post_key,
					);
					$wp_query = new WP_Query($args);

					//marrim te dhenat nese posti eshte valid.
					$status_valid = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_valide' AND `post_id` = $private_id");

					$original_post_key = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_timestamp' AND `wp_postmeta`.meta_value = '$post_key'  AND `wp_postmeta`.post_id = $private_id ");

					if(!empty($wp_query) && $status_valid == 1 && $original_post_key == $post_key) {
						$update_args = array(
							'ID' => $private_id,
							'post_status' => 'draft'
						);
						$status_valid = 0;
						update_post_meta($private_id,'ac_valide', $status_valid);
						wp_update_post($update_args);
						wp_trash_post($private_id);
						$success = "Shpallja është larguar me sukses.";
					}
					else {
						$error[] = "Shpallja nuk ekziston !";
					}
				}

				else {
					$error[] .= "Kanë ndodhur gabime !";
				}
				?>

					<?php
                    if(isset($error) && !empty($error)) {
                       ?>
                     <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: "Kanë ndodhur gabime !",
                                text: "<?php 
                                    foreach($error as $err) {

                                    echo '<ul><li style=\"text-align:center\">'.$err.'</li></ul>';
                                }
                                ?>",
                                styling: 'bootstrap',
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"error",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                    </script>

                    <?php
                    }
                    elseif(isset($success) && $success !== '') {
                    ?>

                    <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: 'Sukses.',
                                text: "<?php  echo '<br /><p>'.$success.'</p>'; ?>",
                                styling: 'bootstrap',
                                icon:"icon-ok icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"success",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
						
                    </script>
                    <?php
                    }
                    //redirect
                    $ac_home = home_url();
					header("refresh:2;url=".$ac_home);
                ?>
            </div><!-- /span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section>

<?php get_footer(); ?>
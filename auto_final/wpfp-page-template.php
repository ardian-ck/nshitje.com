<section class="post-content post-premium-special">
    <div class="row">
        <div class="container">
            <div class="span12 leftZero" id="main" >
                <h3 class="page-header"><span>Shpalljet e ruajtura</span><small class="photoAddTitle" data-toggle="tooltip" title="Për të pasur qasje në shpalljet e ruajtura, duhet të lejoni përdorimin e cookies. Kjo për shkak se faqja jonë shfrytëzon cookies për të mundësuar ruajtjen e posteve me rëndësi për ju. "><i class="icon icon-question" ></i></small></h3>

                <div class="auto_container_grid">
                    <div class="row">
                        <?php 
                        if ($favorite_post_ids):
                        $favorite_post_ids = array_reverse($favorite_post_ids);
                        foreach ($favorite_post_ids as $post_id) {
                            $p = get_post($post_id);
                            //te dhenat
                            $auto_type = get_post_meta($p->ID, 'ac_tipi_automjetit', true);
                            $auto_model = get_post_meta($p->ID, 'ac_modeli', true);
                            $auto_price = get_post_meta($p->ID, 'ac_cmimi_auto', true);
                            $auto_location = get_post_meta($p->ID, 'lokacioni', true);
                            $auto_status = get_post_meta($p->ID, 'ac_cmimi_test', true);
                            $auto_year = get_post_meta($p->ID, 'ac_viti_prodhimit', true);
                            $status = array();
                            if($status) {
                            foreach($auto_status as $s) {
                                switch($s) {
                                    case "0":
                                    $status[] = "Me marrëveshje";
                                    break;

                                    case "1":
                                    $status[] = "Çmimi fiks";
                                    break;


                                    case "2":
                                    $status[] = "Ndërroj";
                                }
                            }
                            }

                            if(is_numeric($auto_location)) {
                                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                                $auto_location = $lok['name'];
                            }
                            $auto_trans = get_post_meta($post->ID, 'transmisioni', true);
                            if(is_numeric($auto_trans)) {
                                $trans = get_term_by('id', $auto_trans, 'transmisioni', 'ARRAY_A');
                                $auto_trans = $trans['name'];
                            }
                            $auto_km = get_post_meta($post->ID, 'ac_km_kaluara', true);
                        ?>
                        <div class="span3">
                            <div class="auto_container">
                                <div class="image">
                                    <div class="content">
                                        <a href="<?php echo get_permalink($post_id); ?>"></a>
                                        <?php echo get_the_post_thumbnail ( $post_id, 'medium');?>
                                        <?php  echo wpfp_remove_favorite_link($post_id); ?>
                                    </div><!-- /.content -->

                                    <div class="price_type">
                                        <?php echo $auto_price; ?> &euro;
                                    </div><!-- /.Ndrroj-Shitet -->
    
                                    <div class="price">
                                        <?php 
                                        if(isset($status)) {
                                            foreach($status as $price_status){
                                                echo 
                                                '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                                           }
                                        }
                                        ?>
                                    </div><!-- /.price -->

                                </div><!-- /.image -->
                                <?php
                                $post_title = $p->post_title;
                                $custom_title = substr($post_title , 0, 30);
                                ?>
                                <div class="title">
                                    <h3>
                                        <a href="<?php echo get_permalink($post_id); ?>"><?php echo $custom_title.'...'; ?></a>
                                    </h3>
                                </div><!-- /.title -->

                                <div class="extra-info">
                                    <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
                                    <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
                                </div>
                            </div><!-- /auto_container -->       
                        </div><!-- /span3 -->
                        <?php 
                        }
                        ?>
                        <?php
                         else:
                            echo "";
                            echo $wpfp_options['favorites_empty'];
                            echo "<br><br>";
                        endif;
                        echo "";
                        ?>
                    </div>
                </div><!-- /.properties-grid -->
            </div><!--/span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /post-content -->
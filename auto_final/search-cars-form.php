<?php
global $wpdb;
?>

<div class="search-form-container">
	<h4 class="bleft bright"><i class="icon-paper-clip paper"></i>Kërkoni automjetin që dëshironi <span class="note"></i></h4>
	<form action="<?php bloginfo('url'); ?>/kerko-automjete/" method="get">
		<ul class="leftZero">
			<li>	
				<div class="clearfix car_search_container">
					<div id="first">	<label for="">Modeli:</label>
					<select name="select_modeli" id="select_modeli"  class="car_select" style="width:35%">
						<option selected="selected" value="">---</option>
						<?php
							
							$results = $wpdb->get_results("SELECT * FROM `wp_auto_list`");
							foreach($results as $res) {
								echo '<option value="'.$res->model.'" data-id="'.$res->id.'">'.$res->model.'</option>';
							}
						?>
					</select>
					</div>

					<div id="second">	<label for="">Seria: <span id="image_loader" class="none"><img src="<?php echo THEMEROOT; ?>/images/468.gif" /></span> </label>
						<select name="select_serie" id="select_serie" class="car_select">
								<option selected="selected" value="">---</option>
						</select>
					</div>
					
					<div id="third"><label for="">Viti:</label>
						<select name="select_year" id="select_year" class="postform">
						<option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option>
						</select>
					</div>

					<div class="clearfix"></div>
				</div>
			</li>

			<li class="clearfix car_search_special">
				<div class="first_group">
					<label for="">Lënda djegëse:</label>
					<?php echo ac_build_select('lenda_djegese'); ?>
				</div>
				<div class="second_group">
					<label for="">Lokacioni:</label>
					<?php echo ac_build_select('lokacioni'); ?>
				</div>

				<div class="third_group">
						<label for="">Transmisioni:</label>
						<?php echo ac_build_select('transmisioni'); ?>
				</div>
			</li>
		</ul>
		<div class="price_select">
			<select class="select_min" name="select_min" style="width:35%">
				<option selected="selected" value="">Çmimi Minimal</option>
				<option value="400">400</option>
				<option value="600">600</option>
				<option value="1000">1000</option>
				<option value="2000">2000</option>
			</select>

			<select class="select_max" name="select_max" style="width:35%">
				<option selected="selected" value="">Çmimi maksimal</option>
				<option value="3000">3000</option>
				<option value="5000">5000</option>
				<option value="7000">7000</option>
				<option value="9000">10000</option>
				<option value="15000">15000</option>
				<option value="55000">15000+</option>
			</select>
			<div class="clearfix"></div>
		</div>
		<div><input type="submit" id="search_cars" class="btn btn-primary btn-large" value="K&euml;rko"><span class="see-all">ose</span><a href="<?php echo get_option('home') ?>/automjetet/" class="btn btn-large btn-danger">Shiko të gjitha shpalljet</a></div>
	</form>
</div>
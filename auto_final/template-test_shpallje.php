<?php
/**
 * Template Name: Test Shto Shpallje
 */
get_header();
?>
<?php
global $wpdb;
$error = array();

$prefix = "ac_";
if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['ac_action']) && $_POST['ac_action'] == "automjete")
     {
    if (isset($_POST['ac_shto_shpallje']) && wp_verify_nonce($_POST['ac_ruaj_shpallje_nonce'], basename(__FILE__))) {
        var_dump($_FILES);
        //marrim te dhenat.
        if ($_POST['ac_shpallja_excerpt'] != '') {
            $ac_shpallja_excerpt = sanitize_text_field($_POST['ac_shpallja_excerpt']);
        } else {
            $error[] .= "Shtoni titullin e shpalljes. Jo më shumë se 100 karaktetere.";
        }

        //taxonomite.
        $lloji = sanitize_text_field($_POST['lloji']);
        $ac_viti_prodhimit = $_POST['cq-year'];
        $ac_tipi_automjetit = $_POST['cq-make'];
        $modeli = $_POST['cq-model'];
        $lenda_djegese = sanitize_text_field($_POST['lenda_djegese']);
        $ac_kubikazhi = sanitize_text_field($_POST['ac_kubikazhi']);
        $transmisioni = sanitize_text_field($_POST['transmisioni']);
        $lokacioni = sanitize_text_field($_POST['lokacioni']);

        $ac_km_kaluara = $_POST['ac_km_kaluara'];
        if ($_POST['ac_cmimi_auto'] != '' && is_numeric($_POST['ac_cmimi_auto'])) {
            $ac_cmimi_auto = sanitize_text_field($_POST['ac_cmimi_auto']);
        } else {
            $error[] .= "Shënoni çmimin e automjetit, duhet të jetë numër pa presje dhjetore apo pikë.";
        }

        $ac_checkbox_cmimi = $_POST['ac_checkbox_cmimi']; //array
        if ($_POST['ac_pershkrimi'] != '') {
            $ac_pershkrimi = esc_textarea($_POST['ac_pershkrimi']);
        } else {
            $error[] .= "Jepni një përshkrim për automjetin.";
        }

        //te dhenat personale.
        if ($_POST['ac_emri_postuesit'] != '') {
            $ac_emri_postuesit = sanitize_text_field($_POST['ac_emri_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni emrin.";
        }

        if ($_POST['ac_email_postuesit'] != '') {
            $ac_email_postuesit = sanitize_email($_POST['ac_email_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni një email valide.";
        }

        if ($_POST['ac_nr_postuesit'] != '') {
            $ac_nr_postuesit = sanitize_text_field($_POST['ac_nr_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni numrin e telefonit.";
        }

        //kjo do te perdoret per te treguar nese posti eshte valid.
        $ac_valide = 1;
        //numri i mundesive per ndryshime, perdoret nga postuesi i shpalljes
        $ac_nr_ndryshimeve = 0;

        $ac_post_submitted_date = the_date('F j, Y g:i a');
        $ac_post_date = current_time('mysql', 0);
        $post_identification = md5(uniqid(rand(), true)); //32 number identification per postin

        //ruajm te dhenat
        $post_data = array(
            'post_title' => wp_strip_all_tags($ac_shpallja_excerpt),
            'post_content' => wp_strip_all_tags($ac_pershkrimi),
            'post_author' => $ac_emri_postuesit . ' (' . $ac_email_postuesit . ')',
            'post_date' => $ac_post_submitted_date,
            'post_status' => 'pending',
            'post_author' => wp_strip_all_tags($ac_email_postuesit),
            'post_type' => 'automjete'
        );

        //shtojme imazhet.
        if ($_FILES) {
            foreach ($_FILES as $file => $array) {
                if (isset($_FILES[$file]) && ($_FILES[$file]['size'] > 0)) {
                    $tmpName = $_FILES[$file]['tmp_name'];
                    list($width, $height, $type, $attr) = getimagesize($tmpName);
                    $arr_file_type = wp_check_filetype(basename($_FILES[$file]['name']));
                    $uploaded_file_type = $arr_file_type['type'];
                    $allowed_file_types = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');
                    if (!in_array($uploaded_file_type, $allowed_file_types)) {
                        $error[] .= "Ju lutem shtoni fajlla të tipit JPG, JPEG, GIF apo PNG";
                    }
                    if(($_FILES[$file]['size'] > 2097152) || $width > 2000 || $height > 2000 ) {
                        $error[] .= "Imazhet janë të mëdha, ju lutem shtoni imazhet deri në 2MB.";
                    }
                } else {
                    $error[] .= "Ju lutem shtoni fotot.";
                }
            }
        }

        if(empty($error)) {
            $post_id = wp_insert_post($post_data);
            if ($post_id) {          
                //shtojm metadata ne post.
                add_post_meta($post_id, 'ac_shpallja_excerpt', $ac_shpallja_excerpt, true);
                add_post_meta($post_id, 'lloji', $lloji, true);
                add_post_meta($post_id, 'tipi', $ac_tipi_automjetit, true);
                add_post_meta($post_id, 'modeli', $modeli, true);
                add_post_meta($post_id, 'ac_viti_prodhimit', $ac_viti_prodhimit, true);
                add_post_meta($post_id, 'lenda_djegese', $lenda_djegese, true);
                add_post_meta($post_id, 'ac_kubikazhi', $ac_kubikazhi, true);
                add_post_meta($post_id, 'transmisioni', $transmisioni, true);
                add_post_meta($post_id, 'ac_km_kaluara', $ac_km_kaluara, true);
                add_post_meta($post_id, 'ac_cmimi_auto', $ac_cmimi_auto, true);
                add_post_meta($post_id, 'ac_cmimi_test', $ac_checkbox_cmimi, true);
                add_post_meta($post_id, 'ac_data_publikimit', $ac_post_date, true);
                add_post_meta($post_id, 'ac_timestamp', $post_identification, true);

                add_post_meta($post_id, 'ac_pershkrimi', $ac_pershkrimi, true);
                add_post_meta($post_id, 'lokacioni', $lokacioni, true);
                add_post_meta($post_id, 'ac_emri_postuesit', $ac_emri_postuesit, true);
                add_post_meta($post_id, 'ac_email_postuesit', $ac_email_postuesit, true);
                add_post_meta($post_id, 'ac_nr_postuesit', $ac_nr_postuesit, true);
                add_post_meta($post_id, 'ac_valide', $ac_valide, true);
                add_post_meta($post_id, 'ac_nr_ndryshimeve', $ac_nr_ndryshimeve, true);

                //update modelin e automjetit
                if($_POST['cq-make'] !== '') {
                $selected_model = strtolower($_POST['cq-make']);
                $terms = get_terms('modeli', 'orderby=count&hide_empty=0');
                //marrim krejt elementet prej custom taxonomy
                $term_name = array();
                foreach($terms as $term) {
                    $term_name[] = $term->slug;
                    if($selected_model == $term->slug) {
                        $term_req = $term->slug;
                    }
                }
                if(in_array($selected_model, $term_name)){
                    //nese $selected_model eshte ne term_name wp_set_post_term
                    //Problemi: Kthe value ne slug dhe jo id sic eshte per momentin.
                    //nese $selected_model = $term_name dhe marrim id nga custom + update.
                    $custom_selected_id = $wpdb->get_var("SELECT `wp_terms`.term_id FROM `wp_terms` WHERE `wp_terms`.slug = '$term_req'");
                    }
                    wp_set_post_terms($post_id, $custom_selected_id, 'modeli');
                }

                //shtojm edhe te dhenat per taxonomite.
                wp_set_post_terms($post_id, $lloji, 'lloji');
                wp_set_post_terms($post_id, $transmisioni, 'transmisioni');
                wp_set_post_terms($post_id, $lokacioni, 'lokacioni');
                wp_set_post_terms($post_id, $lenda_djegese, 'lenda_djegese');

                if ($_FILES) {
                    foreach ($_FILES as $file => $array) {
                        $new_upload = ac_insert_attachment($file, $post_id);
                    }
                }
                wp_set_post_tags($post_id, $_POST['post_tags']);

                $success = 'Pasi të miratohet, shpallja do të jetë në faqen tonë. Klikoni në linkun e dërguar në email-in e shënuar, për të larguar shpalljen.';

                $weburl = get_option('home');

                $message = '<h3>Faleminderit !</h3>';
                $message .= '<p>Shpallja është dërguar me sukses, pasi të miratohet ajo do të jetë në faqen tonë.</p>';
                $message .= '<p>Klikoni në linkun më poshtë nëse dëshironi të <strong>ndryshoni / largoni</strong> shpalljen.</p>';
                $message .= '<p>Për të ndryshuar çmimin e shpalljes mund të klikoni në linkun më poshtë <small>(Kujdes: Keni vetëm 3 raste për të ndryshuar çmimin.) </small></p>';
                $message .= $weburl.'/ndrysho-shpalljen?post='.$post_id.'&id='.$post_identification;
                $message .= '<br /><p>Për të larguar tërësisht shpalljen klikoni në linkun e mëposhtëm: <small>(Kujdes: Shpallja do të fshihet sapo të klikoni në link.) </small> </p>';
                $message .= $weburl.'/konfirmo-fshirjen?post='.$post_id.'&id='.$post_identification;
                            
                //dergojm konfirmimin ne emailin e postuestit
                ac_send_mail($post_id, $ac_email_postuesit, $message);

                unset($_POST);
                $ac_home = home_url();
                //echo "<meta http-equiv='refresh' content='3;url=$ac_home' />"; exit;
                header("refresh:2;url=".$ac_home);
            }//if($post_id)
            else {
               // $error[] .= "Shpallja nuk është shtuar.";
                $error[] .= "Të dhënat e njejta nuk mund të shtohen, përsëri.";
            }
        }//
        else {
           $error[] .= "Shpallja nuk është shtuar.";
        }
    } //if(isset($_POST['ac_shto_shpallje']
    else {
        $error[] .= "Gabim !";
    }
}//if 'POST' = $_SERVER['REQUEST_METHOD']
?>
<section class="listing-container">
    <div class="row">
        <div class="container">
            <div class="span12 listings">
                <?php
                    if(isset($error) && !empty($error)) {
                       
                    ?>
                     <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: "Kanë ndodhur gabime !",
                                text: "<?php 
                                    foreach($error as $err) {

                                    echo '<ul><li>'.$err.'</li></ul>';
                                }
                                ?>",
                                styling: 'bootstrap',
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"error",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                    </script>
                    <?php
                    }
                    elseif(isset($success) && $success !== '') {

                    ?>
                    <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: "Të dhënat u shtuan me sukses",
                                text: "<?php  echo '<br /><p>'.$success.'</p>'; ?>",
                                styling: 'bootstrap',
                                icon:"icon-ok icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"success",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                    </script>
                    <?php
                    }
                    else {
                    ?>
                    <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: "",
                                text: "<?php  echo '<br /><h4>Plotësoni me kujdes të dhënat për të shtuar shpallje.</h4>'; ?>",
                                styling: 'bootstrap', 
                                icon: "icon-info-sign icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:3000,
                                hide:true,
                                history: false,
                                min_height:"150px",
                                animation:"fade",
                                animate_speed: "fast",
                                type:"info",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                    </script>  
                    <?php  
                    }
                ?>
                <div class="error" style="display:none;">
                  <span></span>.<br clear="all"/>
                </div>
                <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST" id="ac_forma_shto_shpallje" name="ac_form_shto_shpallje" enctype="multipart/form-data">
                    <div class="span8 leftZero">
                        <h4>Të dhënat e përgjithshme:</h4>
                        <div class="clearfix">
                            <div class="span4">
                                <label for="ac_shpallja_excerpt">Shpallje për:</label>
                                <input type="text" name="ac_shpallja_excerpt" id="ac_shpallja_excerpt" class="input required" style="height:100%; line-height:auto;" maxlength="50"/>
                            </div>
                            
                            <div class="span4">
                                <label for="lloji">Lloji i automjetit:</label>
                                <?php echo ac_get_categories_dropdown('lloji', 'lloji', $lloji); ?>
                            </div>
                        </div>

                        <div class="clearfix select-cars">
                            <div class="span2 leftZero">
                                <label for="cq-year">Viti prodhimit :</label>
                                <?php
                                echo do_shortcode('[cq-year]');
                                ?>
                            </div>
                            <div class="span3">
                                <label for="cq-make">Lloji i automjetit:</label>
                                <?php
                                echo do_shortcode('[cq-make]');
                                ?>
                            </div>
    
                            <div class="span3 leftZero">
                                <label for="cq-model">Modeli i automjetit :</label>
                                <?php
                                echo do_shortcode('[cq-model]');
                                ?>
                            </div>
                            <!-- <div class="span3 leftZero">
                                <label for="cq-model">Tipi i automjetit :</label>
                                <?php
                                //echo do_shortcode('[cq-trim]');
                                ?>
                            </div> -->
                        </div>

                        <div class="clearfix">
                            <div class="span4">
                                <label for="lenda_djegese">Lënda djegëse:</label>
                                <?php echo ac_get_categories_dropdown('lenda_djegese', 'lenda_djegese', $lenda_djegese); ?>
                            </div>

                            <div class="span4">
                                <label for="ac_kubikazhi">Kubikazhi:</label>
                                <select name="ac_kubikazhi" id="ac_kubikazhi" class="postform">
                                    <option value="1.2">0.8 deri 1.2</option>
                                    <option value="1.4">1.4</option>
                                    <option value="1.6">1.6</option>
                                    <option value="1.8">1.8</option>
                                    <option value="2.0">2.0</option>
                                    <option value="2.2">2.2</option>
                                    <option value="2.4">2.4</option>
                                    <option value="2.6">2.6</option>
                                    <option value="2.8">2.8</option>
                                    <option value="3.0">3.0</option>
                                    <option value="4.0">4.0 e më shumë</option>
                                </select></div>   
                        </div>


                        <div class="clearfix">
                            <div class="span4">
                                <label for="transmisioni">Tipi i transmisionit: </label>
                                <?php echo ac_get_categories_dropdown('transmisioni', 'transmisioni', $transmisioni); ?>
                            </div>

                            <div class="span4">
                                <label for="ac_km_kaluara">Kilometra të kaluara (km): </label>
                                <input type="text" id="ac_km_kaluara" name="ac_km_kaluara" class="input" style="height:100%; line-height:auto;">
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="span4">
                                <label for="ac_cmimi_auto">Çmimi:</label>
                                <input type="text" id="ac_cmimi_auto" name="ac_cmimi_auto" style="height:100%; line-height:auto;">
                            </div>
                            <div class="span4">
                                <label>Çmimi është: </label>
                                <ul class="inputs-list leftZero">
                                    <li> 
                                        <input type="checkbox" name="ac_checkbox_cmimi[0]" id="ac_checkbox_cmimi" value="0" style="float:left;">
                                        Me marrëveshje
                                    </li>
                                    <li>
                                        <input type="checkbox" name="ac_checkbox_cmimi[1]" id="ac_checkbox_cmimi" value="1" style="float:left;">
                                        Çmimi është fiks
                                    </li>
                                    <li>
                                        <input type="checkbox" name="ac_checkbox_cmimi[2]" id="ac_checkbox_cmimi" value="2" style="float:right;">
                                        Bëj ndërrim
                                    </li>
                                </ul>
                            </div><!-- /span4 -->


                        </div>
                    </div><!-- /span 8 -->

                    <div class="span4">
                        <h4>Të dhënat për kontakt:</h4>
                        <div class="span4">
                            <label for="ac_pershkrimi">Përshkrimi i shpalljes: </label>
                            <textarea name="ac_pershkrimi" id="ac_pershkrimi" cols="30" rows="4"></textarea><br />
                        </div>
                        <div class="span4">
                            <label for="lokacioni">Lokacioni:</label>
                            <?php echo ac_get_categories_dropdown('lokacioni', 'lokacioni', $lokacioni); ?>
                        </div>
                        <div class="span4">
                            <label for="ac_emri_postuesit">Emri:</label>
                            <input type="text" id="ac_emri_postuesit" class="required" maxlength="50" name="ac_emri_postuesit" style="height:100%; line-height:auto;">
                        </div>

                        <div class="span4">
                            <label for="ac_email_postuesit">Email:</label>
                            <input type="email" id="ac_email_postuesit" class="required email" maxlength="50" name="ac_email_postuesit" style="height:100%; line-height:auto; width:80%;">
                        </div>

                        <div class="span4">
                            <label for="ac_nr_postuesit">Numri i telefonit:</label>
                            <input type="text" id="ac_nr_postuesit" class="required phone" maxlength="13" name="ac_nr_postuesit" style="height:100%; line-height:auto;">
                        </div>

                    </div><!-- /span 4 -->
                    <div class="span12 leftZero">
                        <?php
                            $upload_dir = wp_upload_dir();
                            $upload_path = $upload_dir['path'] . DIRECTORY_SEPARATOR;
                        ?>
                        <h4>Shto fotot e automjetit:</h4>
                        <div id="photoDrop" class="" data-url="<?php echo $upload_path; ?>">
                            <span class="dz-message">Shto fotot</span>
                            <div id="previews" class="dz-preview"></div>
                        </div>                        
                    </div><!-- /span12 uploader -->
            </div><!-- /listings -->
            <div class="span12">
                 <input type="submit" name="ac_shto_shpallje" id="ac_shto_shpallje" value="Shto shpallje" class="btn btn-large btn-succes main-green frmBtn ignore btnReplace" data-label="Shto shpallje"/>

                 <input type="hidden" name="ac_action" id="ac_action" value="automjete" class="ignore"/>
            <input type="hidden" name="ac_ruaj_shpallje_nonce" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" class="ignore"/>
            </div>
            </form>

            <?php echo do_shortcode('[jquery_file_upload]'); ?>
            <?php jquery_html5_file_upload_hook(); ?>
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /listing-container -->
<?php
//funksionet ndihmese, duhet organizuar kodi, funksionet te barten me nje klase !

/**
 * Funksioni kthen kategorite per taxonomite perkatese.
 * @param type $taxonomy
 * @param type $name
 * @param type $selected
 * @return type
 */
function ac_get_categories_dropdown($taxonomy, $name, $selected) {
    return wp_dropdown_categories(array(
        'taxonomy' => $taxonomy,
        'name' => $name,
        'selected' => $selected,
        'class' => 'postform ignore',
        'hide_empty' => 0,
        'depth' => 1,
        'parent' => 0,
        // 'walker' => new SH_Walker_TaxonomyDropdown(),
        'echo' => 0
    ));
}

/**
 * Funksioni i cili do te shtoje imazhet per secilin post.
 * @param type $file_handler
 * @param type $post_id
 * @param type $setthumb
 * @return type
 */
function ac_insert_attachment($file_handler, $post_id, $setthumb = 'false') {
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK)
        __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload($file_handler, $post_id);
    if ($setthumb) {
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
    }

    update_post_meta($post_id, $file_handler, $attach_id);

    return $attach_id;
}

//$file = $_FILES['file_name']
?>
<?php get_footer(); ?>
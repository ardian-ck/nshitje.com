<?php

/**
 * Klasa kryesore e cila krijon custom postin AUTO se bashku me metaboxet dhe taxonomite perkatese
 */
if (!class_exists('automjete')) {

    class automjete {
        /**
         * Konstruktori
         */
        public function __construct() {
            //funksionet kryesore
            $this->ac_register_post_type();
            $this->ac_register_taxonomies();
            //$this->ac_add_metaboxes();
            
            //funksionet ndihmese
            $this->ac_add_auto_type();
            $this->ac_add_fuel();
            $this->ac_add_locations();
            $this->ac_add_transmision();
            $this->ac_add_auto_model();
            $this->ac_updated_messages();
        }

        /**
         * Funksioni i cili do te krijoje postin AUTO
         */
        public function ac_register_post_type() {
            global $wp_rewrite;
            $labels = array(
                'name' => __('Automjetet'),
                'singular_name' => __('Automjeti'),
                'add_new' => __('Shto shpallje'),
                'add_new_item' => __('Shto shpallje të re'),
                'edit_item' => __('Ndrysho shpalljen'),
                'new_item' => __('Shto një shpallje'),
                'view_item' => __('Shiko shpalljen'),
                'search_items' => __('Kërko shpalljet'),
                'not_found' => __('Nuk ka asnjë shpallje !'),
                'not_found_in_trash' => __('Shpallja nuk ekziston apo është fshirë !'),
                'parent_item_colon' => __('Shpalljet:'),
                'menu_name' => __('Automjetet')
            );

            $args = array(
                'labels' => $labels,
                'description' => 'Shpallje për automjete',
                'public' => true,
                'show_ui' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
                'has_archive' => true,
                'query_var' => 'automjete',
                'can_export' => true,
                'rewrite' => array('slug' => 'automjete', 'with_front' => false),
                'menu_position' => 5,
                'menu_icon' => IMAGESROOT . '/car-red.png',
                'supports' => array(
                    'title',
                    'editor',
                    'thumbnail'
                )
            );

            register_post_type('automjete', $args);
            //Rishkruajm permalink per custom post type.
            $wp_rewrite->use_verbose_page_rules = true;
            add_filter("rewrite_rules_array", array(__CLASS__, 'rewrite_rules_array'));
            add_action("parse_query", array(__CLASS__, 'parse_query'));
            add_filter("post_link_type", array(__CLASS__, 'post_type_link'), 1, 4);
        }
        /**
         * Funksioni i cili mbishkruan permalink per custom post type = automjete
         * @param  [type]  $link
         * @param  [type]  $post
         * @param  boolean $leavename
         * @param  boolean $sample
         * @return [type]
         */
        public function post_type_link($link, $post, $leavename=false, $sample=false) {
            if($sample && ($begin = strpos($link,"?automjete=")) !== false) {
                return substr($link, 0, $begin - 1)."/%automjete%/";
            }
            return str_replace("?automjete=", "", $link)."/";
        }

        /**
         * Funksion ndihmes - mbishkruan permalink per custom post type = automjete
         * @param  [type] $query
         * @return [type]
         */
        public function parse_query($query) {
            global $wp, $wp_rewrite;

            if (isset($query->query['name'])
            && substr($wp->matched_rule, 0, 7) == "([^/]+)"
            && isset($query->query)
            && isset($wp->query_vars)
            && $query->query == $wp->query_vars) {
                if(!($post_types = get_query_var("post_type"))) {
                    if($wp_rewrite->permalink_structure == "/%postname%/")
                        $post_types = array("post");
                    else
                        $post_types = array();
                }

                if(is_array($post_types))
                    $post_types[] = "automjete";
                set_query_var("automjete", $post_types);
            }
        }

        /**
         * Funksion ndihmes - mbishkruan permalink per custom post type = automjete.
         * @param  [type] $array
         * @return [type]
         */
        public function rewrite_rules_array($array) {
            global $wp_rewrite;
            return array_merge($array, $wp_rewrite->generate_rewrite_rules("/%postname%/", EP_PERMALINK));
        }
        /**
         * Funksioni i cili do te krijoje taxonomite per AUTO
         */
        public function ac_register_taxonomies() {
            $taxonomies = array();

            $taxonomies['lloji'] = array(
                'labels' => array(
                    'name' => __('Lloji i automjetit'),
                    'singular_name' => __('Lloji i automjetit'),
                    'edit_item' => __('Ndrysho llojin e automjetit'),
                    'update_item' => __('Përditëso llojin e automjetit'),
                    'add_new_item' => __('Shto lloj të automjetit'),
                    'new_item_name' => __('Shto lloj të automjetit'),
                    'all_items' => __('Të gjitha llojet e automjeteve'),
                    'search_items' => __('Kërko lloje automjete'),
                    'popular_items' => __('Llojet më të kërkuara'),
                    'separate_items_with_comments' => __('Ndaj llojet me presje'),
                    'add_or_remove_items' => __('Shto/largo lloje'),
                    'choose_from_most_used' => __('Zgjedh nga llojet më të përdorura.')
                ),
                'hierarchical' => true,
                'show_ui' => true,
                'query_var' => 'lloji_auto',
                'rewrite' => array('slug' => 'lloji'),
            );

            $taxonomies['modeli'] = array(
                'labels' => array(
                    'name' => __('Modeli i automjetit'),
                    'singular_name' => __('Modeli i automjetit'),
                    'edit_item' => __('Ndrysho modelin e automjetit'),
                    'update_item' => __('Përditëso modelin e automjetit'),
                    'add_new_item' => __('Shto model të automjetit'),
                    'new_item_name' => __('Shto model të automjetit'),
                    'all_items' => __('Të gjitha modelet e automjeteve'),
                    'search_items' => __('Kërko model të automjetit'),
                    'popular_items' => __('Modelet më të kërkuara'),
                    'separate_items_with_comments' => __('Ndaj modelet me presje'),
                    'add_or_remove_items' => __('Shto/largo model'),
                    'choose_from_most_used' => __('Zgjedh nga modelet më të përdorura.')
                ),
                'hierarchical' => true,
                'show_ui' => true,
                'query_var' => 'modeli_auto',
                'rewrite' => array('slug' => 'modeli'),
            );


            $taxonomies['lenda_djegese'] = array(
                'labels' => array(
                    'name' => __('Lënda djegëse'),
                    'singular_name' => __('Lënda djegëse'),
                    'edit_item' => __('Ndrysho lënden djegëse'),
                    'update_item' => __('Përditëso lënden djegëse'),
                    'add_new_item' => __('Shto lënden djegëse'),
                    'new_item_name' => __('Shto lënden djegëse'),
                    'all_items' => __('Të gjitha lëndet djegëse'),
                    'search_items' => __('Kërko sipas lëndes djegëse'),
                    'popular_items' => __('Lëndet djegëse'),
                    'separate_items_with_comments' => __('Ndaj lëndet djegëse me presje'),
                    'add_or_remove_items' => __('Shto/largo lënde djegëse'),
                    'choose_from_most_used' => __('Zgjedh nga lëndet djegëse.')
                ),
                'hierarchical' => true,
                'show_ui' => true,
                'query_var' => 'lenda_djegese',
                'rewrite' => array('slug' => 'lenda_djegese', 'with_front' => false)
            );

            $taxonomies['lokacioni'] = array(
                'labels' => array(
                    'name' => __('Lokacioni i automjetit'),
                    'singular_name' => __('Lokacioni i automjetit'),
                    'edit_item' => __('Ndrysho lokacionin e automjetit'),
                    'update_item' => __('Përditëso lokacionin e automjetit'),
                    'add_new_item' => __('Shto lokacion'),
                    'new_item_name' => __('Shto lokacion'),
                    'all_items' => __('Të gjitha lokacionet'),
                    'search_items' => __('Kërko sipas lokacionit'),
                    'popular_items' => __('Lokacionet më të kërkuara'),
                    'separate_items_with_comments' => __('Ndaj lokacionet me presje'),
                    'add_or_remove_items' => __('Shto/largo lokacion'),
                    'choose_from_most_used' => __('Zgjedh nga lokacionet më të përdorura.')
                ),
                'hierarchical' => true,
                'show_ui' => true,
                'query_var' => 'lokacioni_auto',
                'rewrite' => array('slug' => 'lokacioni', 'with_front' => false)
            );
            
            $taxonomies['transmisioni'] = array(
                'labels' => array(
                    'name' => __('Transmisioni'),
                    'singular_name' => __('Transmisioni'),
                    'edit_item' => __('Ndrysho transmisionin'),
                    'update_item' => __('Përditëso transmisionin'),
                    'add_new_item' => __('Shto transmision'),
                    'new_item_name' => __('Shto transmision'),
                    'all_items' => __('Të gjitha transmisionet'),
                    'search_items' => __('Kërko sipas transmisionit'),
                    'popular_items' => __('Transmisionet më të kërkuara'),
                    'separate_items_with_comments' => __('Ndaj transmisionet me presje'),
                    'add_or_remove_items' => __('Shto/largo transmision'),
                    'choose_from_most_used' => __('Zgjedh nga transmisionet më të përdorura.')
                ),
                'hierarchical' => true,
                'show_ui' => true,
                'query_var' => 'transmisioni',
                'rewrite' => array('slug' => 'transmisioni', 'with_front' => false)
            );
            $this->ac_register_all_taxonomies($taxonomies);
        }

        /**
         * Funksioni i cili do te regjistroje te gjitha taxonomite e krijuara ne funksionin ac_register_taxonomies
         * @param type $taxonomies
         */
        public function ac_register_all_taxonomies($taxonomies) {
            foreach ($taxonomies as $name => $arr) {
                register_taxonomy($name, array('automjete'), $arr);
            }
        }

        /*         * ****** FUNKSIONET NDIHMESE ******** */

        public function ac_init($init) {
            add_action("init", $init);
        }

        public function ac_admin_init($admin_init) {
            add_action("admin_init", $admin_init);
        }

        public function ac_post_filter_message($filter) {
            add_filter("post_updated_messages", $filter);
        }

        public function ac_metabox($metabox) {
            add_action('add_meta_boxes', $metabox);
        }

        /**
         * Funksioni ndihmes i cili do te ndryshoje mesazhet te cilat paraqiten kur ndryshojme postin.
         * @param type $messages
         */
        public function ac_updated_messages() {
            $this->ac_post_filter_message(function() use($messages) {
                        global $post, $post_ID;

                        $messages['automjete'] = array(
                            0 => '',
                            1 => sprintf(__('Të dhënat janë ndryshuar me sukses. <a href="%s"> Shiko automjetin </a>'), esc_url(get_permalink($post_ID))),
                            2 => __('Të dhënat janë ndryshuar.'),
                            3 => __('Të dhënat janë fshirë.'),
                            4 => __('Të dhënat janë përditësuar.'),
                            5 => isset($_GET['revision']) ? sprintf(__('Të dhënat për automjetin janë rishikuar me %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
                            6 => sprintf(__('Të dhënat janë publikuar. <a href="%s"> Shiko postin </a>'), esc_url(get_permalink($post_ID))),
                            7 => __('Të dhënat janë ruajtur'),
                            8 => sprintf(__('Të dhënat janë dërguar. <a target="_blank" href="%s"> Shiko si do të duket posti </a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
                            9 => sprintf(__('Të dhënat janë ruajtur që të publikohen me: <strong>%1$s</strong>. <a traget="_blank" href="%2$s"> Shiko postin </a>'), date_i18n(__('M j Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
                            10 => sprintf(__('Drafti i postit është përditësuar. <a target="_blank" href="%s"> Shiko postin </a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
                        );
                        return $messages;
                    });
        }

        /**
         * Funksioni i cili ne menyre dinamike shton llojet e automjeteve.
         */
        public function ac_add_auto_type() {
            $auto_type = array(
                'Veturë'
            );
            foreach ($auto_type as $type) {
                if (!term_exists($type, 'lloji'))
                    wp_insert_term($type, 'lloji');
            }
        }

        function ac_add_auto_model() {
            $auto_model = array(
    'Abarth','AC','Acura','Alfa Romeo','Allard','Alpina', 'Alpine','Alvis','AMC','Ariel','Armstrong Siddeley', 'Ascari','Aston Martin','Audi','Austin', 'Austin-Healey', 'Autobianchi','Auverland','Avanti', 'Bentley', 'Berkeley', 'Beijing', 'Bitter','Bizzarrini','BMW','Bristol','Brilliance','Bristol', 'Bugatti','Buick', 'Cadillac','Caterham','Checker', 'Chevrolet','Chrysler','Citroen','Dacia','Daewoo','DAF', 'Daihatsu','Daimler','Datsun','De Tomaso','Dodge','Donkervoort', 'Eagle', 'Fairthorpe', 'Ferrari','Fiat','Fisker','Ford','Eagle','GAZ','Geely','Ginetta','GMC','Holden','Honda','Hudson', 'Humber', 'Hummer','Hyundai','Infiniti','Innocenti','Italdesign','Isuzu','Jaguar','Jeep','Jensen','Kia','Koeingsegg','Lada','Lamborghini','Lancia','Land Rover','Lexus','Lincoln','Lotec','Lotus','Luxgen','Mahindra','Marcos','Maserati','Matra-Simca','Maybach','Mazda','MCC','McLaren','Mercedes-Benz','Mercury','MG' ,'Mini' ,'Mitsubishi' ,'Monteverdi','Morgan','Nissan', 'Oldsmobile','Opel','Pagani','Panoz','Peugeot','Pininfarina','Plymouth','Pontiac','Porsche','Proton','Reliant','Renault','Rover','Riley','Rolls Royce','Saab','Samsung','Saturn','Scion','Seat','Simca','Skoda','Smart','Saleen','Spyker','SsangYong','SSC','Steyr','Studebaker','Subaru','Suzuki','Talbot', 'Tata','Tatra','Tesla','Toyota', 'Triumph','TVR','Vauxhall','Venturi','Vector','Volkswagen','Volvo','Wartburg','Westfield','Xedos','Zagato','Zastava','ZAZ','Zenvo','ZIL','Aprilia','Bmw','Buell','Can-Am','Harley-Davidson','Honda','Johnny Pag','Kawasaki','Ktm','Kymco','Triumph','Victory','Yamaha','A.P.C. Motor','Acm','Aluma','American','American Eagle','American Ironhorse','American Motorcycle Co','American Quantum','Apollo Chopper','Aprilia','Ariel','Arlen Ness','Aspt','Atk','Baja Motorsports','Bajaj','Bamx','Benelli','Bennche','Beta','Big Bear Choppers','Big Dog Motorcycles','Big Inch Bikes','Bimota','Blata ','Bmc','Boss Hoss','Bourget','Brammo','Bridgestone','Bsa','Buell','Cagiva','California Motorcycle Co','California Side Car','Campagna','Can-Am','Carefree Custom Cycles','Cfmoto','Champion Sidecar','Champion Trike Con','Christini','Cleveland Cyclewerks','Cobra','Condor','Confederate Motorcycles','Coolster','Covington Cycle City','Daelim','Demon (dmc)','Dft','Diablo','Diamo','Droptail','Ducati','E-Ton','Ebr','Ecocycle','Eddie Trotta','Enfield','Evolution Bike Works','Evolve','Excelsior-Henderson','Exotix Cycle','Flyrite Choppers','Fn','Garelli','Gas Gas','Gem','Hannigan','Hard-Bikes','Hardcore Choppers','Harley-Davidson','Haulmark','Hercules','Hilltop Custom Cycle','Hodaka','Honda','Horex','Husaberg','Husqvarna','Hyosung','Ice Bear','Independence','Indian','Intrepid','Jawa Cz','Jcl','Jianshe','Johnny Pag','Jonway','Joyner','Kaotic Customs','Karavan','Kawasaki','Keeway','Knievel','Kodiak','Kraft/Tech','Ktm','Kymco','Lambretta','Lance','Lehman Trike','Lehman Trikes/Honda','Lehman Trikes/Kawasaki','Linhai','Lucky 7 Choppers','Madami','Maico','Malaguti','Marusho','Matchless','Mid-West Choppers','Moto Guzzi','Motobravo','Motofino','Motor Trike','Motor Trike Conversion','Motus','Mustang','Mv Agusta','Mz','Norton','Orange County Choppers','Oset','Ossa','Paul Yaffe','Peace Sports','Peirspeed','Penton','Piaggio','Pitster Pro','Pocket Bike','Polaris','Polini','Precision Cycle Works','Pro Street','Pro-One','Proper Chopper','Puch','Qingqi','Qlink','Red Horse','Redcat','Redneck Engineering','Redstreak Motors','Ridley','Road Smith','Roketa','Royal Enfield','Royal Ryder','Sachs','Santee','Saxon','Sdg','Sea Doo','Sears','California Scooter Co.','Schwinn Scooters','Genuine Scooter Co.','Shenke Motor','Sherco','ShorelandR','Spcns','Spencer Bowman','Ssr Motorsports','Star Motorcycles','Streamline','Suckerpunch Sallys','Sunbeam','Surgical-Steeds','Suzuki','Swift','Sym','Taotao','Tgb','The Trike Shop','Thoroughbred Moto.','Thunder Cycle Designs','Thunder Mountain','Titan Motorcycle Co.','Tomberlin','Tomos','Triumph','Ultima','Ultra Cycle','United Motors','Universal','Ural','Vectrix','Vengeance','Vento','Vespa','Victoria','Victory','Viper','Von Dutch Kustom','Voyager','West Coast Chopper','Whizzer','Wild West','Wildfire','Xingyue','Xtreme','Yama Buggy','Yamaha','Zero Motorcycles','Zhejiang Xingyue','Zhng','Zieman','Znen','Zongshen','Ammann','BMD','Bobcat','CASE','Dynapac','FAI','Fiat_Hitachi','Fiatallis','Fuchs','Haulotte','JCB','Kaeser','Komatsu','Liebherr','MERLO','Montabert','Ostalo','Putzmeister','Schaeff','Terex','AEBI','AGCO M. FERGUSON','AGRIA','AGRITEC','AGT','ALF','ALFA_LAVAL','AL_KO','ALÖ','AMAZONE','AS_MOTOR','AUTOWRAP','Atlas','BAAS','BAECHT','BARIGELLI','BAUER','BBG','BECKER','BELARUS','BERAL','BERGMANN','BERTI','BLEINROTH','BRIGGS&STRATTON','BRIX','BUCHER','Bourgoin','CAPELLO','CASE_IH','CLAAS','CRAMER','Caeb','DEUTZ_FAHR','DIADEM','DOLL','DUTZI','EBERHARDT','EICHER','FAHR','FEHRENBACH','FELLA','FENDT','FIATAGRI','FLÖTZINGER','Foton','GALLAGHER','GASPARDO','GASSNER','GERINGHOFF','GOTHA','GRASDORF','GRIESSER','GRUBER','GUTBROD','Goldoni','GÜLDNER','HAGEDORN','HARDI','HASSIA','HEYWANG','HOLDER','HONDA','HORSCH','HOWARD','HUSQVARNA','Hanomag','IGLAND','IHC','IMT','ISEKI','Iveco','JF','JUNGHENRICH','John Deere','KEMPER','KLEINE','KRIEGER','KRÖGER','KUHN','KVERNELAND','Kramer','Krone','Kubota','KÄRCHER','KÖCKERLING','KÖLA','Kögel','LAMBORGHINI','LANDSBERG','LAVERDA','LELY','LEMKEN','LEYLAND','LIFTER','LIFTON','LINDNER','LOMBARDINI','Langendorf','MAN','MANITOU','MASCHIO','MAYER','MC HALE','MEELS','MENGELE','MONOSEM','MORRA','MTD','MUH','McCormick','MÖRTL','NEUERO','NEW HOLLAND','NIEMEYER','NODET','Naud','Neuson','O&K','Ostalo','PEGORARO','PERKINS','PLEINTINGER','PZ_VICON','Porsche','PÖTTINGER','QUIVOGNE','RABE','RAU','REFORMWERKE WELS','REGENT','RICONA','ROPA','ROTH','ROTOLAND','Rapid','Reform','Renault','SAME','SAMPO','SAXONIA','SCHLÜTER','SCHMOTZER','SCHRIEVER','SCHUITEMAKER SR HOLLAN','SEKO','SILOKING','SILOKING','STEGSTEDT','STIGA','STIHL','STOLL','Sandtec','Schaeff','Schwarzmüller','Sileo','Steyr','TAARUP','TAURUS','TEBBE','TECNOMA','TECUMSEH','TIM','TIMBERJACK','TOMA VINKOVIC','TORNADO','TORO','TRUMAG','URSUS','VALMET','VICON','VOGEL&NOOT','Volvo','WELGER','WEWELER','WILMS','Weber','ZETOR','ZF','ZUIDBERG','ZWEEGERS','Zmaj','ÖVERUM','DAF','Dacia','Fap','Isuzu','Iveco','Kia','Krone','MAN','Mitsubishi','Ostalo','Piaggio','Suzuki','TAM','Zastava','Škoda'
            );

            foreach($auto_model as $model) {
               if (!term_exists($model, 'modeli'))
                    wp_insert_term($model, 'modeli');
            }
        }


        public function ac_add_locations() {
            $locations = array(
                'Deçan', 'Dragash', 'Ferizaj', 'Fushë Kosovë', 'Gjakovë', 'Gjilan',
                'Drenas', 'Hani Elezit', 'Istog', 'Junik', 'Kaçanik', 'Kamenicë',
                'Klinë', 'Leposaviç', 'Lipjan', 'Malishevë', 'Mamushë', 'Mitrovicë',
                'Novobërdë', 'Obiliq', 'Pejë', 'Podujevë', 'Prishtinë', 'Prizren',
                'Rahovec', 'Shtërpce', 'Shtime', 'Skënderaj', 'Suharekë', 'Viti',
                'Vushtrri', 'Zubin Potok', 'Kllokot', 'Ranillug', 'Graçanicë', 'Partesh'
            );

            foreach ($locations as $location) {
                if (!term_exists($location, 'lokacioni'))
                    wp_insert_term($location, 'lokacioni');
            }
        }

        public function ac_add_fuel() {
            $fuel_list = array(
                'Benzin',
                'Diesel',
                'Gas',
                'Elektricitet',
                'Tjetër'
            );
            foreach ($fuel_list as $fuel) {
                if (!term_exists($fuel, 'lenda_djegese'))
                    wp_insert_term($fuel, 'lenda_djegese');
            }
        }
        
        public function ac_add_transmision() {
            $trans_list = array(
                'Automatik',
                'Gjysëm-automatik',
                'Manual'
            );
            foreach($trans_list as $transmision) {
                if(!term_exists($transmision, 'transmisioni'))
                        wp_insert_term($transmision, 'transmisioni');
            }
        }
    }
      
    // Klasa automjete
}

add_action('init', 'ac_init');

function ac_init() {
    new automjete();
}

?>
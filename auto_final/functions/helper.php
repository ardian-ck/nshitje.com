<?php
function ac_block_bad_queries() {
    global $user_ID;
    if($user_ID) {
      if(!current_user_can('level_10')) {
        if (strlen($_SERVER['REQUEST_URI']) > 255 ||
          strpos($_SERVER['REQUEST_URI'], "eval(") ||
          strpos($_SERVER['REQUEST_URI'], "CONCAT") ||
          strpos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
          strpos($_SERVER['REQUEST_URI'], "base64")) {
            @header("HTTP/1.1 414 Request-URI Too Long");
        @header("Status: 414 Request-URI Too Long");
        @header("Connection: Close");
        @exit;
        }
      }
    }
}
add_action('init', 'ac_block_bad_queries');

/**
 * Funksion ndihmes per te ruajtur imazhet. KUJDES - NUK ESHTE PERDORUR.
 * @param  [type] $file
 * @param  [type] $post_id
 * @return [type]
 */
function ac_image_upload($file, $post_id) {
    if ($file !== '') {
        include_once ABSPATH . "wp-admin/includes/media.php";
        include_once ABSPATH . "wp-admin/includes/file.php";
        include_once ABSPATH . "wp-admin/includes/image.php";

        $upload = wp_handle_upload($file, array('test_form' => false));
        if (!isset($upload['error']) && isset($upload['file'])) {
            $title = $file['name'];
            $file_type = wp_check_filetype(basename($upload['file'], null));
            $ext = strrchr($title, '.');
            $title = ($ext !== false) ? substr($title, 0, -strlen($ext)) : $title;

            $attach_key = '_thumbnail_id';
            $attach_key_image = 'image_path';
            $attachment = array(
                'post_mime_type' => $file_type['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($upload['file'])),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id = wp_insert_attachment($attachment, $upload['file']);
            $existing_download = (int) get_post_meta($post_id, $attach_key, true);
            if (is_numeric($existing_download)) {
                wp_delete_attachment($existing_download);
            }
            $imagepath = wp_get_attachment_image_src($attach_id, 'medium');
            $attach_data = wp_generate_attachment_metadata($attach_id, $upload['file']);
            wp_update_attachment_metadata($attach_id, $attach_data);
            update_post_meta($post_id, 'image_path', $imagepath[0]);
            $success = "Fotot dhenat u shtuan. 1.0";
            echo $success;
        }
    }
    else {
        $error .= "Ju lutem shtoni imazhet.";
        echo $error;
    }
}

/**
 * Funksioni i cili kthen numrin e imazheve per postim.
 * @return [type] [description]
 */
function ac_count_attachments($id) {
    $attachments = get_children(array('post_parent' => $id));
    $count_attachments = count($attachments);
    return $count_attachments;
}


/**
 * Funksioni i cili do te fshije imazhet nga Media Library, pasi posit te jete fshire.
 * @param  [type] $post_id
 * @return [type]
 */
function ac_remove_unattached_images($post_id) {
    global $wpdb;
    $child_atts = $wpdb->get_col("SELECT ID FROM {$wpdb->posts} WHERE post_parent = $post_id AND post_type = 'attachment'");
    foreach($child_atts as $id) {
        wp_delete_attachment($id);
    }
}

add_action('before_delete_post', 'ac_remove_unattached_images');
add_action('trash_post','ac_remove_unattached_images');

//custom post type admin columns
function ac_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

function ac_columns_head($defaults) {
    $defaults['featured_image'] = "Featured Image";
    return $defaults;
}

function ac_columns_content($column_name, $post_ID) {
    if($column_name == 'featured_image') {
        $post_featured_image = ac_get_featured_image($post_ID);
        if($post_featured_image) {
            echo '<img src="'.$post_featured_image.'" alt="" width="100" height="75"/>';
        }
        else {
            echo '<img src="'.get_bloginfo('template_url').'/images/default.png" width="100" height="75" />';
        }
    }
}

add_filter('manage_automjete_posts_columns', 'ac_columns_head', 10);
add_action('manage_automjete_posts_custom_column', 'ac_columns_content', 10, 2);

//rendisim postet sipas numrit te shikimeve, ne backend.
function ac_get_post_views($post_ID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($post_ID, $count_key, true);
    if($count == '') {
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
        return '0 klikime';
    }
    return $count. ' klikime.';
}

function ac_set_post_views($post_ID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($post_ID, $count_key, true);
    if($count == '') {
        $count = 0;
        delete_post_meta($post_ID, $count_key);
        add_post_meta($post_ID, $count_key, '0');
    }
    else {
        $count++;
        update_post_meta($post_ID, $count_key, $count);
    }
}

add_filter('manage_automjete_posts_columns', 'ac_posts_column_views');
add_action('manage_automjete_posts_custom_column', 'ac_custom_column_views', 5, 2);
function ac_posts_column_views($defaults) {
    $defaults['post_views'] = __('Klikimet');
    return $defaults;
}

function ac_custom_column_views($column_name, $id) {
    if($column_name == 'post_views') {
        echo ac_get_post_views(get_the_ID());
    }
}
//Funksioni i cili do te rendit postimet sipas klikimeve.
function ac_posts_column_views_sortable($columns) {
    $columns['post_views'] = 'post_views';
    return $columns;
}
add_filter('manage_edit-automjete_sortable_columns', 'ac_posts_column_views_sortable');

function ac_posts_orderby_column($vars) {
    if(isset($vars['orderby']) && 'post_views_count' == $vars['orderby']) {
        $vars = array($vars, array(
            'meta_key' => 'post_views_count',
            'orderby' => 'meta_value_num',
        ));
    }
    return $vars;
}

add_filter('request', 'ac_posts_orderby_column');

/**
 * Funksion ndihmes per titullin, shkurton ne 50 karaktere max. duke shtuar ...
 * @param  [type]  $str
 * @param  integer $limit
 * @param  boolean $strip
 * @return [type]
 */
function ac_limit_characters($str, $limit = 50, $strip = false) {
    $str = ($strip == true) ? strip_tags($str) : $str;
    if(strlen($str) > $limit ) {
        $str = substr($str, 0, $limit - 3);
        return (substr($str, strrpos($str, ' ')).'...');
    }
    return trim($str);
}

function ac_custom_title($title) {
    if((strlen($title) > 30)) {
        $newTitle = substr($title, 0, 30);
        return $newTitle." &hellip;";
    }
    else {
        return $title;
    }
}

function ac_limit_excerpt() {
   $excerpt = get_the_content();
   $excerpt = strip_shortcodes($excerpt);
   $excerpt = strip_tags($excerpt);
   if(strlen($excerpt) > 50) {
     $the_str = substr($excerpt, 0, 50);
     $the_str = $the_str.'...';
   }
   else {
     $the_str = $excerpt;
   }
   return $the_str;
}
/**
 * Funksioni i cili nuk lejon qe titulli i shpalljes te jete me shume se 35 karaktere.
 * @param  [type] $title [description]
 * @return [type]        [description]
 */
function ac_limit_title($title) {
    if((strlen($title) > 30) && !is_single()) {
        $newTitle = substr($title, 0, 30);
        return $newTitle." &hellip;";
    }
    else {
        return $title;
    }
}
add_filter('the_title', 'ac_limit_title', 10, 1);

/** PJESA E FUNKSIONEVE PER KERKIM NE TE DHENA **/
/**
 * Funksioni i cili kthen listen me te dhena nga taxonomite. 
 * @param  [type] $tax
 * @return [type]
 */
function ac_build_select($tax) {
    $terms = get_terms($tax);
    $x = '<select name="'.$tax.'" class="postform">';
    $x .= '<option value="">-- zgjedh --</option>';
    foreach($terms as $term) {
        $x .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
    }
    $x .= '</select>';
    return $x;
}


if(is_admin()) {
    function ba_admin_posts_filter( $query ) {
            global $pagenow;
            if ( is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
                $query->query_vars['meta_key'] = $_GET['ADMIN_FILTER_FIELD_NAME'];
                $query->query_vars['meta_compare'] = 'LIKE';
                if (isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '') {
                    $query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
                }
            }
        }
        add_filter( 'parse_query', 'ba_admin_posts_filter' );
        function ba_admin_posts_filter_restrict_manage_posts() {
            global $wpdb;
            $sql = 'SELECT DISTINCT meta_key FROM '.$wpdb->postmeta.' ORDER BY 1';
            $fields = $wpdb->get_results($sql, ARRAY_N);
            printf( '<select name="ADMIN_FILTER_FIELD_NAME" title="%s"><option value="">%s</option>', 
                    translate('Zgjedh një fushë për të filtruar të dhënat.', 'baapf'),
                    translate('Filtro sipas fushave', 'baapf')
                   );
            $current = isset($_GET['ADMIN_FILTER_FIELD_NAME'])? $_GET['ADMIN_FILTER_FIELD_NAME']:'';
            $current_v = isset($_GET['ADMIN_FILTER_FIELD_VALUE'])? $_GET['ADMIN_FILTER_FIELD_VALUE']:'';
            foreach ($fields as $field) {
                if (substr($field[0],0,1) != "_"){
                printf( '<option value="%s"%s>%s</option>',
                        $field[0],
                        $field[0] == $current? ' selected="selected"':'',
                        $field[0]
                    );
                }
            }
            printf( '</select> %s<input type="TEXT" name="ADMIN_FILTER_FIELD_VALUE" value="%s" title="%s"/>', 
                    translate('Value:', 'baapf'),
                    $current_v,
                    translate('Lëreni të zbrazët nëse dëshironi të kërkoni nëpër të gjitha postet.', 'baapf')
                  );
        }
        add_action( 'restrict_manage_posts', 'ba_admin_posts_filter_restrict_manage_posts' );
        /* Register a custom column (based on: @see http://scribu.net/wordpress/custom-sortable-columns.<a href="http://it.knightnet.org.uk/tags/html" class="st_tag internal_tag" rel="tag" title="Posts tagged with HTML">html</a>)
            and adds it to the edit-post page
            Changed to pick up the filter column specified above (if it is), if nothing selected, nothing is added
         */
        function jk_custom_column_register( $columns ) {
            if ( isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
                $columns[$_GET['ADMIN_FILTER_FIELD_NAME']] = __( $_GET['ADMIN_FILTER_FIELD_NAME'], 'my-plugin' );
            }
            return $columns;
        }
        add_filter( 'manage_edit-post_columns', 'jk_custom_column_register' );
        // Display the column content on the manage posts page
        function jk_custom_column_display( $column_name, $post_id ) {
            if ( isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
                if ( $_GET['ADMIN_FILTER_FIELD_NAME'] != $column_name )
                    return;
              
                $custcol = get_post_meta($post_id, $_GET['ADMIN_FILTER_FIELD_NAME'], true);
                if ( !$custcol )
                    $custcol = '<em>' . __( 'undefined', 'my-plugin' ) . '</em>';
              
                echo $custcol;
            }
        }
        add_action( 'manage_posts_custom_column', 'jk_custom_column_display', 10, 2 );
        // How should WP sort this column?
        function jk_custom_column_orderby( $vars ) {
            if ( isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
                if ( isset( $vars['orderby'] ) && $_GET['ADMIN_FILTER_FIELD_NAME'] == $vars['orderby'] ) {
                    $vars = array_merge( $vars, array(
                        'meta_key' => $_GET['ADMIN_FILTER_FIELD_NAME'],
                        'orderby' => 'meta_value'  // 'meta_value_num' or 'meta_value'
                    ) );
                }
            }
            return $vars;
        }
        add_filter( 'request', 'jk_custom_column_orderby' );
        // Register some extra columns as sortable on the edit posts page
        function jk_column_register_sortable( $columns ) {
            if ( isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
                $columns[$_GET['ADMIN_FILTER_FIELD_NAME']] = $_GET['ADMIN_FILTER_FIELD_NAME'];
            }
            $columns['categories'] = 'categories';   
            // $columns['tags'] = 'tags'; // doesn't work correctly as tags are multi-valued?
            return $columns;
        }
        add_filter( 'manage_edit-post_sortable_columns', 'jk_column_register_sortable' );
}

function ac_pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Faqja ".$paged." nga ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; Faqja e parë</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Faqja paraprake</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Faqja Tjetër &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Faqja e fundit &raquo;</a>";
         echo "</div>\n";
     }
}


/**
 * Funksioni i cili do te dergoje emailin e konfirmimit shfrytezuesit.
 * @param  [type] $post_id
 * @param  [type] $user_email
 * @param  [type] $message
 * @return [type]
 */
function ac_send_mail($post_id, $user_email, $message) {
    include_once ABSPATH . "/wp-includes/class-phpmailer.php";
    global $error;
    $mail = new PHPMailer();
    
    $username = "test@nshitje.com";
    $password = "Makavelli1989";
    $subject = "Mesazh nga faqja !";

    $mail->IsSMTP();
    $mail->SMTPDebug = 1;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tsl';
    $mail->Host = "am2.siteground.biz";
    $mail->Port = 25;
    $mail->Username = $username;
    $mail->Password = $password;
    $mail->Priority = 1;
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = '8bit';
    $mail->ContentType = 'text/html; charset=utf-8\r\n';
    $mail->Subject = "Mesazh nga faqja";
    $mail->SetFrom("test@nshitje.com", "Mesazh nga faqja");
    $mail->AddAddress($user_email);
    $mail->IsHTML(true);
    $mail->MsgHTML($message);
    $mail->WordWrap    = 900;
    $mail->Body = $message;
     

    if (!$mail->Send()) {
        $error = 'Mail error: ' . $mail->ErrorInfo;
        return false;
    } else {
        $error = 'Message sent!';
        $mail->SmtpClose();
        return true;

    }
}

/**
 * Funksion i cili manualisht zevendeson hour->ore dhe days->dite.Zgjidhje e perkoheshme.
 * @param  [type] $from [description]
 * @param  string $to   [description]
 * @return [type]       [description]
 */
function ac_custom_time($from, $to ='') {
    $diff = human_time_diff($from, $to);
    $replace = array(
        'hour' => 'orë',
        'mins' => 'min',
        'hours' => 'orë',
        'day' => 'ditë',
        'days' => 'ditë',
        'week' => 'javë',
        'weeks' => 'javë',
        'month' => 'muaj',
        'months' => 'muajve'
    );
    return strtr($diff, $replace);
}


function ac_unique_array($array, $value) {
    $count = 0;
    foreach($array as $array_key => $array_value) {
        if(($count > 0) && ($array_value == $value)) {
            unset($array[$array_key]);
        }
        if($array_value == $value) $count++;
    }
    return array_filter($array);
}
?>
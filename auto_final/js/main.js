jQuery(window).load(function(){
  $('#status').fadeOut();
  $('#preloader').delay(350).fadeOut('slow');
        $('.views #stats').addClass('label');
  function imgLoaded(img){  
    $(img).parent().addClass('loaded');
  }
  
});

jQuery(document).ready(function($){
    $('ul.news-scroller').liScroll({
    travelocity: 0.07
    });
    
    $('span#image_loader').hide();
    
    $('select#select_modeli').minimalect({
      onchange:function(value, text) {
        var model_id = value;
        //console.log(model_id);
  
        $.ajax({
          type:"POST",
          url:main_ajax.ajaxurl,
          data: {action:"get_serie_init", model_id : model_id },
          beforeSend: function() {
            $('span#image_loader').fadeIn();
          },
          
          complete: function() {
            $('span#image_loader').fadeOut();
          },
          
          success:function(response) {
            $('select#select_serie').html(response);
          }
        });
      }
    });

    $('.postform, .select_min, .select_max').minimalect();
});
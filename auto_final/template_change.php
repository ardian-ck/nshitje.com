<?php
/**
*	Template Name: Ndrysho shpalljen
*/
?>
<?php get_header(); ?>

<section class="listing-container">
    <div class="row">
        <div class="container">
            <div class="span12 listings">
				<?php 
				global $wpdb;

				$error = array();

				if(isset($_GET['id']) && strlen($_GET['id']) == 32) {
					$post_id = intval($_GET['post']);
					$post_key = $_GET['id'];
                    $private_id = ($post_id - 825920496) / 1498158;

                    $price = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_cmimi_auto' and `wp_postmeta`.post_id = $private_id");

					if(isset($_POST['ac_ndrysho_shpallje']) && wp_verify_nonce($_POST['ac_ndrysho_shpallje'], basename(__FILE__))) {
                        if(is_numeric($_POST['ac_cmimi'])) {
                            $new_price = esc_attr(intval($_POST['ac_cmimi']));
                        }
                        else {
                            $error[] .= "Çmimi duhet të jetë numër, pa presje apo shenja tjera.";
                        }
					//marrim te dhenat per postin me id dhe post key nga url.
					$args = array(
						'post_type' => 'automjete',
						'meta_key' => 'ac_timestamp',
						'meta_value' => $post_key,
					);
    					$wp_query = new WP_Query($args);

    					//marrim te dhenat nese posti eshte valid.
    					$status_valid = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_valide' AND `post_id` = $private_id");

    					$original_post_key = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_timestamp' AND `wp_postmeta`.meta_value = '$post_key'  AND `wp_postmeta`.post_id = $private_id ");

    					$nr_changes = $wpdb->get_var("SELECT `wp_postmeta`.meta_value FROM `wp_postmeta` WHERE `wp_postmeta`.meta_key = 'ac_nr_ndryshimeve' AND `post_id` = $private_id");

    					// var_dump($nr_changes);//fillim 0
    					// var_dump($status_valid);//fillim 1
    					// var_dump($original_post_key);//ok
    					// var_dump($price);//

    					if(!empty($wp_query) && $status_valid == 1 
                            && $original_post_key == $post_key && $nr_changes <= 3 && empty($error)) {
    						//ndrysho cmimin & ndrysho nr changes ne $nr_changes + 1
                            //cmimi i vjeter 
                            update_post_meta($private_id, 'ac_cmimi_ndryshuar_auto', $price);
                            //cmimi i ndryshuar
    						update_post_meta($private_id,'ac_cmimi_auto', $new_price);
                            $nr_changes++;
    						update_post_meta($private_id, 'ac_nr_ndryshimeve', $nr_changes);
    						$success = "Shpallja është ndryshuar me sukses.";
    					}
                        else {
                            $error[] .= "Nuk mund të ndryshoni shpalljen.";
                        }
                    }//isset
				}
				else {
					$error[] .= "Kanë ndodhur gabime !";
				}
				?>

					<?php
                    if(isset($error) && !empty($error)) {
                       ?>
                     <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: "Kanë ndodhur gabime !",
                                text: "<?php 
                                    foreach($error as $err) {

                                    echo '<ul><li style=\"text-align:center\">'.$err.'</li></ul>';
                                }
                                ?>",
                                styling: 'bootstrap',
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"error",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                    </script>

                    <?php
                    }
                    elseif(isset($success) && $success !== '') {
                    ?>

                    <script type="text/javascript">
                        $(function() {
                            $.pnotify({
                                title: 'Sukses.',
                                text: "<?php  echo '<br /><p>'.$success.'</p>'; ?>",
                                styling: 'bootstrap',
                                icon:"icon-ok icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"success",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
						
                    </script>
                    <?php
                    }
                    //redirect
                    $ac_home = home_url();
					//header("refresh:2;url=".$ac_home);
                ?>
                <div class="span5 offset4">
                    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST" id="ac_ndrysho" name="ac_ndrysho">
                    <label for="ac_cmimi">Ndrysho çmimin:</label>
                    <input type="text" name="ac_cmimi" id="ac_cmimi" maxlength="6" size="6" style="height:100%; line-height:auto;" placeholder="çmimi aktual: <?php echo $price; ?> &euro;" /><br />
                    <input type="submit" name="ac_ndrysho_shpallje" id="ac_ndrysho_shpallje" value="Ruaj" class="btn btn-large btn-succes main-green frmBtn ignore btnReplace" data-label="Ruaj"/>
                    <input type="hidden" name="ac_ndrysho_shpallje" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" class="ignore"/>
                </form>
                </div>
            </div><!-- /span12 -->
        </div><!-- /container -->
    </div><!-- /row -->
</section>

<?php get_footer(); ?>
<?php 
/**
* Template Name: Automjetet Test Faqe
*/
get_header(); 
?>

<?php
//marrim te dhenat
global $post;

if(get_query_var('paged')) $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
if(get_query_var('page')) $paged = (get_query_var('page')) ? get_query_var('page') : 1;

$args = array(
'post_type' => array('automjete'),
'post_status' => 'publish',
'posts_per_page' => 8,
'orderby' => 'date',
'order' => 'DESC',
'paged' => $paged
);
$wp_query = null;
$wp_query = new WP_Query($args);
$total_pages = $wp_query->max_num_pages;

?>
<script type="text/javascript">
//<![CDATA[
function imgLoaded(img){  
    $(img).parent().addClass('loaded');
  }
//]]>
</script>
<section class="post-content-special">
<div class="row">
  <div class="container">
    <div class="span12 leftZero">
    <div class="wrapper wf">
      <!-- BEGIN CONTROLS -->
      <nav class="controls just">
        <div class="group" id="Sorts">
          <div class="button active" id="ToGrid"><i></i>Rrjetë</div>
          <div class="button" id="ToList"><i></i>Listë</div>
        </div>
        <div class="group btn btn-danger" id="Filters">
          <div class="drop_down wf">
            <span class="anim150">Lloji i mjetit</span>
            <ul class="anim250">
              <li class="active" data-filter="all" data-dimension="region">Të gjitha</li>
              <?php
              $args = array('taxonomy' => 'lloji', 'hide_empty' => false);
              $terms = get_terms('lloji', $args);
              // var_dump($terms);
              foreach($terms as $term) {
              ?>
              <li data-filter="<?php echo strtolower($term->name); ?>" data-dimension="region"><?php echo ucfirst($term->name); ?></li>
               <?php
              }
              ?>
            </ul>
          </nav><!-- END CONTROLS -->     
      <!-- BEGIN PARKS -->
      <ul id="Parks" class="just leftZero">
        <!-- "TABLE" HEADER CONTAINING SORT BUTTONS (HIDDEN IN GRID MODE)-->
        <div class="list_header">
          <div class="meta name active desc" id="SortByName">
            Rendit shpalljet sipas alfabetit &nbsp;
            <span class="sort anim150 asc active" data-sort="data-name" data-order="desc"></span>
            <span class="sort anim150 desc" data-sort="data-name" data-order="asc"></span>  
          </div>
          <div class="meta region">Lloji</div>
          <div class="meta area" id="SortByArea">
            Rendit sipas çmimit &nbsp;
            <span class="sort anim150 asc" data-sort="data-area" data-order="asc"></span>
            <span class="sort anim150 desc" data-sort="data-area" data-order="desc"></span>
          </div>
        </div>
        
        <!-- FAIL ELEMENT -->
        
        <div class="fail_element anim250">Nuk ka asnjë shpallje me kriteret që keni kërkuar !</div>
        
        <?php
            if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post();
            ?>
            <?php 
              $auto_price = get_post_meta($post->ID, 'ac_cmimi_auto', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              $cmimi_auto = get_post_meta($post->ID, 'ac_cmimi_auto', true);
              $cmimi_test = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              if(is_numeric($auto_location)) {
                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                $auto_location = $lok['name'];
              }
              $auto_status = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_year = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
              $status = array();
              foreach($auto_status as $s) {
                  switch($s) {
                      case "0":
                      $status[] = "Me marrëveshje";
                      break;

                      case "1":
                      $status[] = "Çmimi fiks";
                      break;


                      case "2":
                      $status[] = "Ndërroj";
                  }
              }
            ?>

        <!-- BEGIN LIST OF PARKS (MANY OF THESE ELEMENTS ARE VISIBLE ONLY IN LIST MODE)-->
        <?php 
        $terms_list = get_the_term_list($post->ID, 'lloji', '', '', '');

        ?>
        <li class="mix <?php echo strtolower(strip_tags($terms_list)); ?>" data-name="<?php the_title(); ?>" data-area="<?php echo $auto_price; ?>">
          <div class="meta name">
            <div class="img_wrapper">
              <?php
                //$url = wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID), 'medium');
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
                $url = $thumb[0];$url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
              ?>
              <img src="<?php echo $url; ?>" onload="imgLoaded(this)"/>
            </div>
            <div class="titles">
              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <p><em><?php the_excerpt(); ?></em></p>
            </div>
          </div>
          <div class="meta region">
            <p><?php echo get_the_term_list($post->ID, 'lloji', '','',''); ?></p>
          </div>
          <div class="meta area">
            <div>
              <p><?php echo $auto_price; ?>&euro;</p>
            </div>
          </div>

          <div class="meta grid_price_status">
                <?php 
                if(isset($status)) {
                    foreach($status as $price_status){
                        echo 
                        '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                   }
                }
                ?>
          </div>

          <div class="meta extra-info">
              <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
              <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
          </div>
        </li>
        
        <?php 
      endwhile;
        ?>
        
        <?php
        else:
        ?>
      <p>Nuk ka asnje post.</p>
      <?php
      endif;
      wp_reset_query();
      ?>
      </ul>
    </div> <!-- END DEMO WRAPPER -->
    <?php 
    if(function_exists("ac_pagination")) {
      ac_pagination($total_pages);
    } 
  ?>
</div><!--/span12 -->
</div><!-- /container -->
</div><!-- /row -->
</section><!-- /post-content -->
<?php get_footer(); ?>

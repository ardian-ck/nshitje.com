<footer class="main-footer">
	<div class="row">
		<div class="container">
			<div class="span12">
				<div class="span4 twitter leftZero">
					<h4>Faqet</h4>
				            <ul>
				             <?php
				              wp_list_pages(
				                array(
				                  'exclude' => '13,11,7,1961',
				                  'title_li' => '',
				                )
				              );
				             ?>
				            </ul>
					<div class="tweets"></div>
				</div><!-- / -->

				<div class="span4 leftZero">
					<h4>Kategoritë</h4>
					<ul>
					   <?php
				              wp_list_categories('title_li=&taxonomy=lloji');
				            ?>
					</ul>
				</div><!-- / -->

				<div class="span4 info leftZero">
					<h4>Informata për kontakt</h4>
					<ul>
					 <li>Facebook: facebook.com/nshitje</li>
					 <li>Twitter: @nshitje</li>
					 <li>Email:<a href="mailto:info@nshitje.com"> info@nshitje.com</a></li>
					</ul>
				</div><!-- / -->
			</div>
		</div>
		<div class="row">
			<div class="container">
				<div class="span12 copyright-container">
					<div class="copyright">
						<p>Të gjitha të drejtat e rezervuara &copy; <?php the_date('Y'); ?>, Prishtinë</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
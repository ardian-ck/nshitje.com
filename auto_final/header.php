<?php  
if ( substr_count( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' ) ) {  
    ob_start( "ob_gzhandler" );  
}  
else {  
    ob_start();  
}
?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <meta name="description" content="<?php the_excerpt_rss(); ?>" />
    <?php endwhile; endif; else: ?>
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <?php endif; ?>
    <?php if(is_single() || is_page() || is_home()) { ?>
    <meta name="googlebot" content="index,noarchive,follow,noodp" />
    <meta name="robots" content="all,index,follow" />
    <meta name="msnbot" content="all,index,follow" />
    <?php } else { ?>
    <meta name="googlebot" content="noindex,noarchive,follow,noodp" />
    <meta name="robots" content="noindex,follow" />
    <meta name="msnbot" content="noindex,follow" />
    <?php }?> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title><?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); } ?></title>
    <!--[if lt IE 9]>
    <script src="dist/html5shiv.js"></script>
    <![endif]-->
    <script>
   //<![CDATA[
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-44229459-1', 'nshitje.com');
  ga('send', 'pageview');
    //]]>
   </script>
<?php wp_head(); ?>
</head>
<?php flush(); ?>
<body <?php body_class(); ?>>
<?php if(!is_page('shpallje')): ?>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<?php endif; ?>
<header id="page-header">
    <div class="main-header">
        <div class="row">
            <div class="container">
                <div class="span12 leftZero">
                <div class="span6">
                    <nav id="top-navigation">
                        <ul class="navlinks leftZero">
                            <li><a href="#">Rreth nesh</a></li>
                            <li><a href="#modalHelp" role="button" data-toggle="modal">Ndihm&euml;</a></li>
                        </ul>
                    </nav>
                </div>

                <div id="social" class="span6 leftZero">
                    <div id="social-buttons" class="">
                    <ul>
                        <li><a href="https://www.facebook.com/nshitje" id="facebook" target="blank"><i  class="icon icon-facebook icon-2x"></i></a></li>
                        <li><a href="https://twitter.com/nshitje" id="twitter" target="blank"><i class="icon icon-twitter icon-2x"></i></a></li>
                        <li><a href="#" id="google"><i  class="icon icon-google-plus icon-2x" target="blank"></i></a></li>
                    </ul>
                    </div>
                </div><!-- /social -->
                </div><!-- /span12 -->
            </div><!-- /container -->
        </div><!-- /row -->
    </div><!-- /main-header -->

    <div class="main-menu">
        <div class="row">
            <div class="container">
                <div class="span12">
                    <div class="span2 logo-container leftZero" id="wrapper">
                    	<a href="<?php bloginfo('url'); ?>"><img src="<?php echo THEMEROOT; ?>/images/logo_car122.png" /></a>
                    </div><!-- /span3 -->

                    <div class="span7">
                        <div class="navbar">
                          <div class="navbar-inner">
                            <div class="container">
                         
                              <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                              <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </a>
                         
                         
                              <!-- Everything you want hidden at 940px or less, place within here -->
                              <div class="nav-collapse collapse main-navigation">
                                <!-- .nav, .navbar-search, .navbar-form, etc -->
                                <ul class="leftZero effect">
                                    <li><a href="<?php bloginfo('url'); ?>">Faqja Kryesore</a></li>
                                    <li><a href="<?php echo get_option('home') ?>/automjetet/">Shpalljet e postuara</a></li>
                                    <li><a href="<?php echo get_option('home') ?>/shpalljet-ruajtura/">Shpalljet e ruajtura</a></li>
                                </ul>
                              </div>
                         
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="span3 home_add">
                        <a href="<?php echo get_option('home') ?>/shpallje/" class="btn btn-success btn-large main-green" style="color:#fff;">Shtoni shpallje</a>
                    </div><!-- /span3 -->
                </div><!-- /span12 -->
            </div><!-- /container -->
        </div><!-- /row -->
    </div><!-- /main-menu -->
</header><!-- /main-header -->

<?php include(TEMPLATEPATH.'/help-modal.php'); ?>


<?php if(is_home()): ?>
<section class="news-content">
    <div class="container">
    <?php 
    //get latest posts
    $query = array(
    'post_type' => 'automjete',
    'posts_per_page' => 20,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC'
    );
    $top_posts = new WP_Query($query);
    ?>
    <?php if($top_posts->have_posts()) : ?>
   
        <div class="span12 leftZero">
            <ul class="news-scroller">
                 <?php while($top_posts->have_posts()) : $top_posts->the_post(); ?>
                <li><span><em><?php //echo ac_custom_time(get_the_time('U'), current_time('timestamp')).' më parë' ;?></em></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                 <?php endwhile; ?>
            </ul>
        </div>
        <?php endif; wp_reset_query(); ?>
    </div>
</section><!-- /news-content -->
<?php endif; ?>